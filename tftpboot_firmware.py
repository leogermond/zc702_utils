#!/usr/bin/env python3

import os
import re
import select
import socket
import serial
import subprocess
import sys
import time
from datetime import datetime
import logging
from pathlib import Path
import argparse

from shutil import copyfile
import toml

CUR_DIR = Path(__file__).resolve().parent

log = logging.getLogger()


class Timeout(Exception):
    def __init__(self, msg, ptrn, current_out):
        Exception.__init__(self, msg)
        self.ptrn = ptrn
        self.current_out = current_out


class Console(object):
    STARTUP = 0
    BOOTED = 1
    LOADED = 2
    EXECUTED = 3

    def __init__(self, dev, prompt, ipaddr, serverip, log_file=None):
        self.dev = dev
        self.prompt = prompt
        self.ipaddr = ipaddr
        self.serverip = serverip
        self.stage = self.STARTUP
        log.info(f"opening tty")
        self.tty = serial.Serial(
            port=dev,
            baudrate=115200,
            bytesize=serial.EIGHTBITS,
            stopbits=serial.STOPBITS_ONE,
            parity=serial.PARITY_NONE,
            timeout=1,
        )
        self.tty.reset_output_buffer()
        self.tty.reset_input_buffer()
        self.tty.read()
        self._logger = log_file
        # init output generator
        self._read = self._read_fn()
        self._out = ""

        log.info(f"check for prompt")
        self.command("")
        try:
            self.expect(prompt, 5)
        except Timeout:
            self._wait_autoboot()
        self.stage = self.BOOTED
        self.output = None
        self.status = None

    def _wait_autoboot(self):
        log.warning(f"waiting for boot")
        self.wait_prompt(10)
        self.stage = self.BOOTED

    def tftpboot(self, img, addr=0x200000):
        assert self.stage == self.BOOTED
        retries = 0
        self.command("setenv loadaddr %x" % addr)
        self.wait_prompt()
        self.command("setenv bootfile %s" % img)
        self.wait_prompt()
        self.command("setenv serverip %s" % self.serverip)
        self.wait_prompt()
        self.command("setenv ipaddr %s" % self.ipaddr)
        self.wait_prompt()
        self.command("dcache off")
        self.wait_prompt()
        self.command("icache off")

        while True:
            log.info(f"tftpboot")
            self.command("tftpboot $loadaddr $bootfile")
            match = self.expect(
                "(Bytes transferred|ARP Retry count exceeded)", timeout=30
            )
            if match.group(0).startswith("Bytes transferred"):
                break
            log.warning(f"retry")
            retries += 1
            assert retries < 3, "Issue with TFTP"
        match = self.wait_prompt()
        self.stage = self.LOADED

    def bootelf(self):
        assert self.stage == self.LOADED, "unexpected state: %d" % self.stage
        self._logger.write("\nstart time: %s\n" % datetime.now().strftime("%H:%M:%S"))
        self.command("bootelf $loadaddr")
        self.expect(r"## Starting application at 0x[0-9A-Fa-f]* [.]+\r?\n", timeout=10)
        try:
            match = self.expect(
                re.compile(r"^## Program ended with code ([0-9]+)", flags=re.M),
                timeout=cfg.get("board", {}).get("timeout", 10),
            )
            self.status = int(match.group(1))
            self.output = self._discarded
            self._wait_autoboot()
        except Timeout:
            self._logger.write("## Timeout...")
            self.status = -1
        self._logger.write("\nend time: %s\n" % datetime.now().strftime("%H:%M:%S"))
        self.stage = self.EXECUTED

    def bootm(self):
        assert self.stage == self.LOADED, "unexpected state: %d" % self.stage
        self._logger.write("\nstart time: %s\n" % datetime.now().strftime("%H:%M:%S"))
        self.command("go $loadaddr")
        self.expect(r"## Starting application at 0x[0-9A-Fa-f]* [.]+\r?\n", timeout=10)
        try:
            match = self.expect(
                re.compile(r"^## Program ended with code ([0-9]+)", flags=re.M),
                timeout=700,
            )
            self.status = int(match.group(1))
            self.output = self._discarded
            self._wait_autoboot()
        except Timeout:
            self._logger.write("## Timeout...")
            self.status = -1
        self._logger.write("\nend time: %s\n" % datetime.now().strftime("%H:%M:%S"))
        self.stage = self.EXECUTED

    def command(self, cmd):
        log.debug(f"$ {cmd}");
        self.tty.write(("%s\r\n" % cmd).encode("ascii"))

    def expect(self, ptrn, timeout=5):
        log.debug(f"expect {ptrn!r}")
        if isinstance(ptrn, str):
            ptrn = re.compile(ptrn)
        start = time.monotonic()
        while True:
            if timeout > 0 and time.monotonic() - start > timeout:
                log.debug(f"timeout\n{self._out}")
                raise Timeout(
                    'Timeout (%d s) when looking for "%s"' % (timeout, ptrn),
                    ptrn,
                    self._out,
                )

            m = ptrn.search(self._out)
            if m is not None:
                self._discarded = self._out[0 : m.start()]
                log.debug(f"{self._out}")
                self._out = self._out[m.end() + 1 :]
                return m

            c = next(self._read)
            if c is not None:
                self._out += c

    def wait_prompt(self, timeout=5):
        self.expect(self.prompt, timeout)

    def on_output(self, out):
        sys.stdout.write(out)
        sys.stdout.flush()

    def _read_fn(self):
        while True:
            ready = select.select([self.tty], [], [], 10.0)[0]
            if self.tty in ready:
                out = self.tty.read(1024).decode("iso-8859-15")
                if self._logger is not None:
                    # For debug purpose
                    self._logger.write(out)
                    self._logger.flush()
                for c in out:
                    yield (c)
            else:
                yield (None)


def execute(cmd_line):
    # print('$ %s' % ' '.join(cmd_line))
    subprocess.check_output(cmd_line)


def get_ip():
    IP = cfg.get("tftp", {}).get("ip")
    if IP is None:
        s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        s.settimeout(0)
        try:
            # doesn't even have to be reachable
            s.connect(("10.254.254.254", 1))
            IP = s.getsockname()[0]
        except Exception:
            IP = "127.0.0.1"
        finally:
            s.close()

    return IP


def main(cfg, param):
    log.info("start program")
    base, ext = os.path.splitext(os.path.basename(param))

    if ext == ".adb":
        log.info(f"compiling {ext}")
        elf = "%s.elf" % base
        cmd_line = [
            "gprbuild",
            param,
            "-f",
            "-o",
            elf,
        ]
        execute(cmd_line)
    else:
        elf = param
        log.info(f"elf is {param}")

    base, ext = os.path.splitext(os.path.basename(elf))
    load_elf = (ext != ".bin")
    file = elf

    with open("%s.exec_log" % param, "w") as fp:
        log.info(f"opening console")
        console = Console(
            cfg["board"]["console"],
            cfg["board"]["prompt"],
            cfg["board"]["ip"],
            get_ip(),
            log_file=fp,
        )
        log.info(f"upload")
        console.tftpboot(os.path.basename(file))

        log.info(f"boot")
        if load_elf:
            console.bootelf()
        else:
            console.bootm()

        if console.status != 0:
            raise subprocess.CalledProcessError(
                returncode=console.status, cmd=[elf], output=console.output, stderr=None
            )
        else:
            return console.output


if __name__ == "__main__":
    with open(CUR_DIR / "tftpboot_firmware.toml") as f:
        cfg = toml.load(f)

    ap = argparse.ArgumentParser()
    ap.add_argument("application")
    ap.add_argument("--verbose", "-v", action="store_true")
    args = ap.parse_args()

    logging.basicConfig(level=logging.INFO if not args.verbose else logging.DEBUG)
    sys.stdout.write(main(cfg, args.application))
